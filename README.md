# Switch Theme

For a full description of the module, visit the
[project page](https://www.drupal.org/project/switch_theme).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/switch_theme).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- In appearance tab, click on sub-tab Switch Theme and add a new one
- Choose the role concerned by the theme switching
- Write URI Regex pattern, if the current URI match with the pattern, the choosen theme above will be use
- Choose the theme to apply


## Maintainers

- Raphael COLBOC - [racol](https://www.drupal.org/u/racol)

The drupal module switch_theme was developed by Raphael Colboc (drupalname:
racol).
