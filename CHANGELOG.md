# Changelog

## [1.3.0] - 2024-09-17
### Added
- Drupal 11 support

## [1.2.0] - 2023-08-23
### Changed
- Fix PHPCS errors
- Allowed query parameters for regex theme configuration
- Add configuration link in admin module list for switch theme

## [1.1.0] - 2023-02-14
### Added
- CHANGELOG.md

### Changed
- Cardinality about relationships: one role = URL(s) REGEX = one theme
- Admin form to administrate switchtheme: checkbox replaces by radio buttons for the role concerned
- Replace of fetchAll()[0] method to fetchObject() in SwichthemeManager to avoid error and gain performance
- Cast data in SwichthemeManager/DeleteForm/EditForm to avoid error and potentials security risks
- Removing Drupal 8 support, the module will still maintained for Drupal 9 & 10
