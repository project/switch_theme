<?php

/**
 * PHP version 8
 *
 * @category  SwitchthemeManager
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/SwitchthemeManagerInterface.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme;

use Drupal\Core\Database\Connection;

/**
 * Switch theme Manager
 * 
 * Database manager.
 * 
 * @category SwitchthemeManager
 * @package  SwitchthemeManager
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/SwitchthemeManager.php
 * @see      https://www.drupal.org/project/switch_theme
 */
class SwitchthemeManager implements SwitchthemeManagerInterface
{

    /**
     * Database connection.
     *
     * @var \Drupal\Core\Database\Connection
     */
    protected $connection;

    /**
     * Initialize Database connection.
     * 
     * @param $connection Connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get Switch themes.
     *
     * @return array
     */
    public function getSwitchThemes()
    {

        $query = $this->connection->select('switch_theme', 's');
        $query->innerJoin('switch_theme_role', 'sr', 's.id = sr.switch_theme_id');
        $query->innerJoin('switch_theme_pattern', 'sp', 's.id = sp.switch_theme_id');
        $query->innerJoin('switch_theme_theme', 'st', 's.id = st.switch_theme_id');
        $query->fields('s', ['id']);
        $query->fields('sp', ['pattern']);
        $query->fields('st', ['theme_code']);
        $query->fields('sr', ['role_code']);

        $results = $query->execute()
            ->fetchAll();

        $return = [];
        foreach ($results as $result) {
            $return[] = [
              'pattern'     => $result->pattern,
              'theme_code'  => $result->theme_code,
              'role_code'   => $result->role_code,
            ];
        }

        return $return;
    }

    /**
     * Get item.
     *
     * @param int $id Switch theme id.
     *
     * @return object|null
     */
    public function getItem($id)
    {

        $query = $this->connection->select('switch_theme', 's');
        $query->fields('s', ['name']);
        $query->innerJoin('switch_theme_pattern', 'sp', 's.id = sp.switch_theme_id');
        $query->innerJoin('switch_theme_theme', 'st', 's.id = st.switch_theme_id');
        $query->fields('sp', ['pattern']);
        $query->fields('st', ['theme_code']);
        $query->condition('s.id', (int) $id, "=");
        return $query->execute()
            ->fetchObject();
    }

    /**
     * Get selected roles.
     *
     * @param int $id Switch theme id.
     *
     * @return array
     */
    public function getSelectedRole($id)
    {

        return $this->connection->select('switch_theme_role')
            ->fields('switch_theme_role', ['role_code'])
            ->condition('switch_theme_id', (int) $id, "=")
            ->execute()
            ->fetchObject();
    }
}
