<?php
/**
 * PHP version 8
 *
 * @category  SwitchthemeController
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Controller/SwitchthemeController.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Switch theme Controller
 * 
 * Configure switch theme edit form.
 * 
 * @category SwitchthemeController
 * @package  SwitchthemeController
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Controller/SwitchthemeController.php
 * @see      https://www.drupal.org/project/switch_theme
 */
class SwitchthemeController extends ControllerBase
{

    /**
     * The database connection.
     *
     * @var \Drupal\Core\Database\Connection
     */
    protected $connection;

    /**
     * {@inheritdoc}
     * 
     * @param ContainerInterface $container Symfony Static container
     * 
     * @return Object
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('database')
        );
    }

    /**
     * Initialize DB connection
     * 
     * @param Connection $connection The db connection object.
     *
     * @return void
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Overview.
     *
     * @return array
     */
    public function overview()
    {
        $rows = [];

        $header = [
          [
            'data' => $this->t('ID'),
            'field' => 'st.id',
            'class' => [RESPONSIVE_PRIORITY_MEDIUM],
          ],
          [
            'data' => $this->t('Name'),
            'field' => 'st.name',
            'class' => [RESPONSIVE_PRIORITY_MEDIUM],
          ],
          [
            'data' => $this->t('Operations'),
            'class' => [RESPONSIVE_PRIORITY_LOW],
          ],
        ];

        $query = $this->connection->select('switch_theme', 'st')
            ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
            ->extend('\Drupal\Core\Database\Query\TableSortExtender');
        $query->fields(
            'st', [
            'id',
            'name',
            ]
        );

        $items = $query
            ->limit(50)
            ->orderByHeader($header)
            ->execute();

        foreach ($items as $item) {
            // Re-initialized links array for each row.
            $links = [];

            // The row information displayed for the current user.
            $row = [
              $item->id,
              $item->name,
            ];

            // Fill OPERATIONS links.
            $links['edit'] = [
              'title' => $this->t('Edit'),
              'url'   => Url::fromRoute('switch_theme.edit', ['id' => $item->id]),
            ];

            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url'   => Url::fromRoute('switch_theme.delete', ['id' => $item->id]),
            ];

            $row[] = [
              'data' => [
                '#type'   => 'operations',
                '#links'  => $links,
              ],
            ];

            $rows[] = $row;
        }

        $build['switch_theme_table'] = [
          '#type'     => 'table',
          '#header' => $header,
          '#rows'     => $rows,
          '#empty'     => $this->t('No switch theme available.'),
        ];
        $build['switch_theme_pager'] = ['#type' => 'pager'];

        return $build;
    }
}
