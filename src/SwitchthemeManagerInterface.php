<?php
/**
 * PHP version 8
 *
 * @category  SwitchthemeManagerInterface
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/SwitchthemeManagerInterface.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme;

/**
 * Switch theme Manager Interface
 * 
 * Negociate the current theme depending of the current URI.
 * 
 * @category SwitchthemeManagerInterface
 * @package  SwitchthemeManagerInterface
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/SwitchthemeManagerInterface.php
 * @see      https://www.drupal.org/project/switch_theme
 */
interface SwitchthemeManagerInterface
{

    /**
     * Get switch themes.
     *
     * @return array
     */
    public function getSwitchThemes();

    /**
     * Get switch theme.
     * 
     * @param int $id Theme ID
     *
     * @return array|null
     */
    public function getItem($id);

    /**
     * Get selected roles.
     *
     * @param int $id Role ID
     * 
     * @return array|null
     */
    public function getSelectedRole($id);

}
