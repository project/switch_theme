<?php

/**
 * PHP version 8
 *
 * @category  ThemeNegotiator
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Theme/ThemeNegotiator.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\switch_theme\SwitchthemeManager;

/**
 * Theme Negotiator
 * 
 * Negociate the current theme depending of the current URI.
 * 
 * @category ThemeNegotiator
 * @package  ThemeNegotiator
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Theme/ThemeNegotiator.php
 * @see      https://www.drupal.org/project/switch_theme
 */
class ThemeNegotiator implements ThemeNegotiatorInterface
{
    /**
     * Theme to negociate.
     *
     * @var string
     */
    protected $theme = "";

    /**
     * Role code of administrator.
     *
     * @var string
     */
    protected $administratorRoleCode = 'administrator';

    /**
     * The switch theme manager.
     *
     * @var \Drupal\switch_theme\SwitchthemeManager
     */
    protected $switchemeManager;

    /**
     * Construct.
     *
     * @param \Drupal\switch_theme\SwitchthemeManager $switchemeManager Manager
     *
     * @return void
     */
    public function __construct(SwitchthemeManager $switchemeManager)
    {
        $this->switchemeManager = $switchemeManager;
    }

    /**
     * Apply the rule
     * 
     * @param \Drupal\Core\Routing\RouteMatchInterface $route_match Match route
     *  
     * @return bool
     */
    public function applies(RouteMatchInterface $route_match)
    {

        $currentUser = \Drupal::currentUser();

        $userRoles = $currentUser->getRoles();

        if (in_array($this->administratorRoleCode, $userRoles)) {
            $this->theme = \Drupal::config('system.theme')->get('admin');
            return true;
        }

        $switchthemes = $this->switchemeManager->getSwitchThemes();

        $requestUri = \Drupal::request()->getRequestUri();

        foreach ($switchthemes as $theme) {
      
            $intersect = in_array($theme['role_code'], $userRoles);
            
            preg_match($theme['pattern'], $requestUri, $matches);

            if (!empty($intersect) && !empty($matches)) {
                $this->theme = $theme['theme_code'];
                return true;
            }
        }

        return false;
    }

    /**
     * Determine Active Theme.
     *
     * @param \Drupal\Core\Routing\RouteMatchInterface $route_match Active theme
     *
     * @return string
     */
    public function determineActiveTheme(RouteMatchInterface $route_match)
    {
        return $this->theme;
    }
}
