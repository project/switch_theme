<?php

/**
 * PHP version 8
 *
 * @category  DeleteForm
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/DeleteForm.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;

/**
 * EditForm
 * 
 * Configure switch theme edit form.
 * 
 * @category DeleteForm
 * @package  DeleteForm
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Form/DeleteForm.php
 * @see      https://www.drupal.org/project/switch_theme
 */
class DeleteForm extends ConfirmFormBase
{

    /**
     * The database connection.
     *
     * @var \Drupal\Core\Database\Connection
     */
    protected $connection;

    /**
     * Item switchtheme id to delete.
     *
     * @var int
     */
    protected $id;

    /**
     * Construct.
     *
     * @param Connection $connection The database connection.
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Create.
     *
     * @param ContainerInterface $container The container object.
     * 
     * @return Object
     */
    public static function create(ContainerInterface $container)
    {
        return new static($container->get('database'));
    }

    /**
     * {@inheritdoc}
     * 
     * @return String
     */
    public function getFormId()
    {
        return 'switch_theme_admin';
    }

    /**
     * {@inheritdoc}
     * 
     * @return void
     */
    public function getQuestion()
    {
        return $this->t('Do you really want to remove this switch theme ?');
    }

    /**
     * {@inheritdoc}
     * 
     * @return void
     */
    public function getCancelUrl()
    {
        return new Url('switch_theme.overview');
    }

    /**
     * {@inheritdoc}
     * 
     * @param $form       array              Form
     * @param $form_state FormStateInterface Current form state
     * @param $id         int                Current switch theme form ID
     * 
     * @return Object
     */
    public function buildForm(
        array $form, 
        FormStateInterface $form_state, 
        $id = null
    ) {
        $this->id = (int) $id;
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     * 
     * @param $form       array              Form
     * @param $form_state FormStateInterface Current form state
     * 
     * @return void
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        try {
            $this->connection
                ->delete('switch_theme')
                ->condition('id', $this->id)
                ->execute();
            $this->connection
                ->delete('switch_theme_role')
                ->condition('switch_theme_id', $this->id, "=")
                ->execute();
            $this->connection
                ->delete('switch_theme_pattern')
                ->condition('switch_theme_id', $this->id, "=")
                ->execute();
            $this->connection
                ->delete('switch_theme_theme')
                ->condition('switch_theme_id', $this->id, "=")
                ->execute();

            $this->messenger()
                ->addMessage($this->t('The switch theme has been deleted'));
        }
        catch (\Exception $e) {
            $this->messenger()->addMessage($e->getMessage(), 'error');
        }

        $response = Url::fromRoute('switch_theme.overview');
        $form_state->setRedirectUrl($response);
    }
}
