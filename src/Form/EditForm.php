<?php

/**
 * PHP version 8
 *
 * @category  EditForm
 * @package   Switch_Theme
 * @author    Racol <contact@rcowebdev.com>
 * @copyright 2023 Racol
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link      https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/EditForm.php
 * @see       https://www.drupal.org/project/switch_theme/releases/1.0.x-dev
 */

namespace Drupal\switch_theme\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Url;
use Drupal\user\Entity\Role;
use Drupal\switch_theme\SwitchthemeManager;

/**
 * EditForm
 * 
 * Configure switch theme edit form.
 * 
 * @category EditForm
 * @package  EditForm
 * @author   Racol <contact@rcowebdev.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     https://git.drupalcode.org/project/switch_theme/-/blob/1.0.x/src/Form/EditForm.php
 * @see      https://www.drupal.org/project/switch_theme
 */
class EditForm extends FormBase
{

    /**
     * The database connection.
     *
     * @var \Drupal\Core\Database\Connection
     */
    protected $connection;

    /**
     * The switch theme manager.
     *
     * @var \Drupal\switch_theme\SwitchthemeManager
     */
    protected $switchemeManager;

    /**
     * Switch theme id.
     *
     * @var int|null
     */
    protected $id;

    /**
     * Construct.
     *
     * @param \Drupal\Core\Database\Connection        $connection       DB.
     * @param \Drupal\switch_theme\SwitchthemeManager $switchemeManager Switch theme.
     *
     * @return void
     */
    public function __construct(
        Connection $connection, 
        SwitchthemeManager $switchemeManager
    ) {
        $this->connection = $connection;
        $this->switchemeManager = $switchemeManager;
    }

    /**
     * Create.
     *
     * @param ContainerInterface $container The container object.
     * 
     * @return Object
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('database'),
            $container->get('switch_theme.manager'),
        );
    }

    /**
     * {@inheritdoc}
     * 
     * @return string
     */
    public function getFormId()
    {
        return 'switch_theme';
    }

    /**
     * Validate submitted form.
     *
     * @param array              $form       The form object.
     * @param FormStateInterface $form_state The form state object.
     *
     * @return void
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $roles = Role::loadMultiple();
        $themes = \Drupal::service('theme_handler')->listInfo();

        if (strlen($form_state->getValue('switch_theme_name')) == 0) {
            $form_state->setErrorByName(
                'switch_theme_name', 
                $this->t('You must specify a name.')
            );
        }

        $role = !in_array($form_state->getValue('switch_theme_role'), $roles);

        if (!$role) {
            $form_state->setErrorByName(
                'switch_theme_role', 
                $this->t('The role does not exist.')
            );
        }

        if (strlen($form_state->getValue('switch_theme_pattern')) == 0) {
            $form_state->setErrorByName(
                'switch_theme_pattern', 
                $this->t('You must specify a pattern.')
            );
        }

        if (!in_array(
            $form_state->getValue('switch_theme_theme'), 
            array_keys($themes)
        )
        ) {
            $form_state->setErrorByName(
                'switch_theme_theme', 
                $this->t('The theme does not exist.')
            );
        }
    }

    /**
     * Build Switchtheme form.
     *
     * @param array                                $form       The form object
     * @param \Drupal\Core\Form\FormStateInterface $form_state The form state object
     * @param int|null                             $id         Switch theme
     *
     * @return array
     */
    public function buildForm(
        array $form, 
        FormStateInterface $form_state, 
        $id = null
    ) {
        $this->id = $id;
        $editedItem = $this->switchemeManager->getItem($id);
        $selectedRole = $this->switchemeManager->getSelectedRole($id);

        $roles = Role::loadMultiple();
        $themes = \Drupal::service('theme_handler')->listInfo();

        $options = [];
        foreach ($roles as $role) {
            $options[$role->id()] = $role->label();
        }

        $form['switch_theme_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Name'),
            '#default_value' => ($editedItem) ? $editedItem->name : "",
            '#description' => $this->t('Define switch theme name'),
            '#required' => true,
        ];

        $form['switch_theme_role'] = [
          '#type' => 'radios',
          '#title' => $this->t('Role'),
          '#options' => $options,
          '#description' => $this->t('Which role is concernerd ?'),
          '#default_value' => ($selectedRole) ? $selectedRole->role_code : null,
          '#required' => true,
        ];

        $form['switch_theme_pattern'] = [
          '#type' => 'textfield',
          '#title' => 'URI Pattern',
          '#default_value' => ($editedItem) ? $editedItem->pattern : "",
          '#description' => $this->t('Define URI pattern'),
          '#required' => true,
        ];

        $options = [];
        foreach ($themes as $id => $theme) {
            $options[$id] = $theme->getName();
        }

        $form['switch_theme_theme'] = [
            '#type' => 'radios',
            '#title' => $this->t('Theme'),
            '#options' => $options,
            '#description' => $this->t('Which theme to used ?'),
            '#default_value' => ($editedItem) ? $editedItem->theme_code : "",
            '#required' => true,
        ];

        $form['submit'] = [
            '#type'             => 'submit',
            '#title'            => $this->t('Save'),
            '#default_value'    => "Save",
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     * 
     * @param array              $form       the form object
     * @param FormStateInterface $form_state the current form state
     * 
     * @return void
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $name = $form_state->getValue('switch_theme_name');
        $role = $form_state->getValue('switch_theme_role');
        $pattern = $form_state->getValue('switch_theme_pattern');
        $theme = $form_state->getValue('switch_theme_theme');
        $id = $this->id;

        if ($id > 0) {
            $this->connection->update('switch_theme')
                ->fields(['name' => $name])
                ->condition('id', $id, "=")
                ->execute();
        } else {
            $id = $this->connection->insert('switch_theme')
                ->fields(['name' => $name])
                ->execute();
        }

        $this->connection->delete('switch_theme_role')
            ->condition('switch_theme_id', $id, '=')
            ->execute();

        $this->connection->delete('switch_theme_pattern')
            ->condition('switch_theme_id', $id, '=')
            ->execute();

        $this->connection->delete('switch_theme_theme')
            ->condition('switch_theme_id', $id, '=')
            ->execute();

        $this->connection->insert('switch_theme_role')
            ->fields(['switch_theme_id' => $id, 'role_code' => $role])
            ->execute();

        $this->connection->insert('switch_theme_pattern')
            ->fields(['switch_theme_id' => $id, 'pattern' => $pattern])
            ->execute();

        $this->connection->insert('switch_theme_theme')
            ->fields(['switch_theme_id' => $id, 'theme_code' => $theme])
            ->execute();

        $this->messenger()->addMessage($this->t('The Switch theme has been saved'));

        $response = Url::fromRoute('switch_theme.overview');
        $form_state->setRedirectUrl($response);
    }
}
